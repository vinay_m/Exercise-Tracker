import React, { Component } from 'react';
import Axios from 'axios'

export default class CreateUser extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userName: ''
        }
        this.onChangeUser = this.onChangeUser.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }
    componentDidMount() {

    }

    onChangeUser(e) {
        this.setState({
            userName: e.target.value
        })
    }
    onSubmit(e) {
        e.preventDefault();
        const user = {
            username: this.state.userName
        }
        Axios.post('http://localhost:5000/users/add', user)
            .then(res => console.log(res.data))

        console.log(user);
        this.setState({ userName: '' })
        //  window.location = 
    }
    render() {
        return (
            <div>
                <h3>Create New User</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label>User Name:</label>
                        <input
                            type='text'
                            required
                            className="form-control"
                            value={this.state.userName}
                            onChange={this.onChangeUser}
                        />
                    </div>
                    <div className="form-group">
                        <input type="submit" value="Create User" className="btn btn-primary" />
                    </div>
                </form>
            </div>
        )
    }
}
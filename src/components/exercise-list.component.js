import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Axios from 'axios'

const Exercise = props => {
    return (
        <tr>
            <td>{props.exercise.username}</td>
            <td>{props.exercise.description}</td>
            <td>{props.exercise.duration}</td>
            <td>{props.exercise.date.substring(0, 10)}</td>
            <td><Link to={"/edit/" + props.exercise._id}> Edit</Link> | <a href="#" onClick={() => props.deleteExercise(props.exercise._id)} >Delete</a> </td>
        </tr>)
}

export default class ExerciseList extends Component {
    constructor(props) {
        super(props);
        this.onDelete = this.onDelete.bind(this);
        this.state = { exercises: [] }
    }
    componentDidMount() {
        Axios.get('http://localhost:5000/exercises/')
            .then(res => {
                this.setState({ exercises: res.data })
                // console.log(res.data[0])
            })
            .catch((err) => { console.log(err) })
    }
    onDelete(id) {
        Axios.delete('http://localhost:5000/exercises/' + id)
            .then(response => { console.log(response.data) })
        this.setState({ exercises: this.state.exercises.filter(el => el._id !== id) })
    }
    exerciseList() {
        return this.state.exercises.map((currentExercise) => {
            return <Exercise exercise={currentExercise} deleteExercise={this.onDelete} key={currentExercise._id} />
        })
    }
    render() {
        return (
            <div>
                <h3>Logged Exercises</h3>
                <table className="table">
                    <thead className="thead-light">
                        <tr>
                            <th>User Name</th>
                            <th>Description</th>
                            <th>Duration</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.exerciseList()}
                    </tbody>
                </table>
            </div>
        )
    }
}